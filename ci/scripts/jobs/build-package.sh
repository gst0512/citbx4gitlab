
job_main() {
    local pkgtype=${CI_JOB_NAME##*-}
    local version=$(cat VERSION)

    mkdir -p artifacts build/staging/{bin,lib/ci-toolbox,share/bash-completion/completions}
    sed -E 's/^(CITBX_VERSION=).*$/\1'"$(cat VERSION)"'/g' ci-toolbox/ci-toolbox.sh \
        > build/staging/lib/ci-toolbox/ci-toolbox.sh
    cp ci-toolbox/*.py build/staging/lib/ci-toolbox/
    chmod +x build/staging/lib/ci-toolbox/ci-toolbox.sh
    cp .external/ci-scripts/common.sh build/staging/lib/ci-toolbox/common.sh
    cp .ci-toolbox.properties build/staging/lib/ci-toolbox/ci-toolbox.properties.default
    cp -r ci-toolbox/env-setup ci-toolbox/3rdparty build/staging/lib/ci-toolbox/
    cp wrapper/ci-toolbox build/staging/bin/ci-toolbox
    chmod +x build/staging/bin/ci-toolbox
    cp wrapper/bashcomp build/staging/share/bash-completion/completions/ci-toolbox

    local fpm_args pkgfile
    case "$pkgtype" in
        deb)
            pkgfile=artifacts/ci-toolbox-$version.deb
            rm -f $pkgfile
            fpm_args+=(
                --depends debianutils
                -v $version
                -t deb --deb-priority optional --category admin
                --deb-no-default-config-files
                -p artifacts/ci-toolbox.deb --deb-compression xz
                --after-install packaging/scripts/postinst.deb
                --depends ca-certificates
            )
            ;;
        rpm)
            pkgfile=artifacts/ci-toolbox-$version.rpm
            rm -f $pkgfile
            fpm_args+=(
                --depends which
                -v ${version//-/_}
                -t rpm --rpm-os linux
                --rpm-compression xz
                --after-install packaging/scripts/postinst.rpm
            )
            ;;
        tar)
            pkgfile=artifacts/ci-toolbox-$version.tar.xz
            tar -C build/staging/ -cpJf $pkgfile .
            ;;
        *)
            print_critical "Unsupported package type: '$pkgtype'"
            ;;
    esac
    if [ "$pkgtype" != "tar" ]; then
        fpm_args+=(
            -s dir
            -n ci-toolbox
            -p $pkgfile
            --url https://gitlab.com/mbedsys/citbx4gitlab
            --description "CI toolbox for Gitlab CI"
            -m "Emeric Verschuur <emeric@mbedsys.org>"
            --license "GPLv3"
            --vendor "MBEDSYS"
            --conflicts ci-toolbox
            --provides ci-toolbox
            --replaces ci-toolbox
            --depends bzip2
            --depends file
            --depends git
            --depends git-lfs
            --depends python3-yaml
            --depends tar
            -a all
            packaging/root/=/
            build/staging/=/usr/
        )
        fpm "${fpm_args[@]}"
    fi
    if [ "$CI_COMMIT_REF_PROTECTED" == "true" ] && [ -n "$CI_COMMIT_TAG" ]; then
        curl -sq --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $pkgfile \
            "$CI_API_V4_URL/projects/mbedsys%2fcitbx4gitlab/packages/generic/ci-toolbox/$version/${pkgfile##*/}"
    fi
}

job_after() {
    rm -rf build
}
