# citbx4gitlab: CI toolbox for Gitlab

This toolbox can be used to

* run Gitlab-CI jobs outside a runner on your workstation (classical use case)
* write the script part using a modular format (advanced use case)

## Table of contents

* [The project](#the-project)
    * [ci-toolbox tool setup](#ci-toolbox-tool-setup)
    * [Setup of prerequisites](#setup-of-prerequisites)
    * [ci-toolbox tool update](#ci-toolbox-tool-update)
    * [Optional run-job-script.sh integration in a project](#optional-run-job-scriptsh-integration-in-a-project)
* [Use case: Standard Gitlab-CI pipeline job](#use-case-standard-gitlab-ci-pipeline-job)
* [Run a specific job locally](#run-a-specific-job-locally)
    * [First solution: using gitlab-runner tool](#first-solution-using-gitlab-runner-tool)
    * [Second solution: Use ci-toolbox command](#second-solution-use-ci-toolbox-command)
* [Pipeline command](#pipeline-command)
* [Write a job or a module script using the run-job-script.sh component](#write-a-job-or-a-module-script-using-the-run-job-scriptsh-component)
    * [Write a job](#write-a-job)
    * [Write a module](#write-a-module)
    * [Useful function list](#useful-function-list)
* [Job examples](#job-examples)
    * [job-minimal](#job-minimal)
    * [job-with-before-after-script](#job-with-before-after-script)
    * [job-advanced](#job-advanced)
    * [job-test-services-mysql](#job-test-services-mysql)
    * [job-test-services-postgres](#job-test-services-postgres)

## The project

### ci-toolbox tool setup

You can install this tool using the suitable package from the last release [here](https://gitlab.com/mbedsys/citbx4gitlab/-/releases).

### Setup of prerequisites

The `ci-toolbox setup` command will install and configure your environment with the tool prerequisites
Type `ci-toolbox setup -h` to display the associated help, or `ci-toolbox setup -i` to activate the interactive mode

### ci-toolbox tool update

You can update simply with the command `ci-toolbox setup --component ci-tools` or `ci-toolbox setup --component ci-tools <custom_version>`

### Optional run-job-script.sh integration in a project

Case of integration of `run-job-script.sh` into your project:
```
├── .gitlab-ci.yml
├── .ci-toolbox.properties
├── ci/scripts
│   ├── modules
│   │   ├── ccache.sh
│   │   └── example.sh
│   ├── jobs
│   │   └── job-advanced.sh
│   ├── common.sh
│   └── run-job-script.sh
```

The element list:

* `.gitlab-ci.yml`: Gitlab-CI pipeline definition
* `run-job-script.sh` : entry point to run the job script
* `.ci-toolbox.properties`: CI toolbox project specific properties
* `jobs`: directory containing the job specific functions
* `modules`: directory containing the modules

## Use case: Standard Gitlab-CI pipeline job

The following schema describe the standard execution of a job called "J" in a Gitlab-CI pipeline

![Global use case](doc/GitlabCIPipelineJob.png)

In this case it will be interesting to run a specific job like J in their suitable environment on your local workstation without commit and push.

The aim is to have the exactly the same build environment on the Gitlab runner and the local workstation

![Simple job use case](doc/GitlabCISingleJob.png)

## Run a specific job locally

### First solution: using gitlab-runner tool

To use this solution you have to:

* install gitlab-runner runner tool on the workstation
* `gitlab-runner exec <executor type> <job name>`

In addition you have to:

* Commit the changes to be taken in board by the gitlab-runner
* Add gitlab-runner executor option like `--docker-image`

**NOTE:** `gitlab-runner exec` command is now deprecated since Gitlab 10, and can be removed from next version

### Second solution: Use ci-toolbox command

This tool is able to:

* Start a docker executor with the suitable parameters (image, etc.)
* Run job without having to commit the local changes (using the command `ci-toolbox <my_job_name> [args...]`)
* Run pipelines (see [Pipeline command](#pipeline-command))

Additional advanced ci-toolbox tool functionalities (using `run-job-script.sh`):

* Add workstation specific parameters and actions
* Add ability to run a shell into the suitable job docker environment
* Split job parts in modular format (Job parts, modules, ...)

Usage:

* `ci-toolbox <command> [arguments...]` from anywhere in the project directory

Know limitations:

* Only applicable for docker and shell executor

## Pipeline command

The pipeline command (`ci-toolbox pipeline [arguments...]`) can be used to:
* run the default pipeline with whole the jobs founds in the .gitlab-ci.yml using the command `ci-toolbox pipeline`
* run a custom/specific pipeline using the command `ci-toolbox pipeline -n <my_pipeline_name>`

The custom pipelines must be specified into the file `.ci-pipelines.yml` using the following format:

```YAML
simple custom pipeline:
    - my job 1
    - my job 2
    - my job 5

advanced custom pipeline:
    - my job 3
    - my job 5
    - name: my job 2
      arguments:
        argument-1: "custom value"
        argument-2: ["1st custom value", "2nd custom value"]
```

The YAML document is an object containing the pipelines where:
* each key is corresponding to the pipeline name
* each value is an array of jobs

Each jobs can be represented by:
* A string containing the job name
* An object containing a `name` and `arguments` with the custom argument list

An example of `.ci-pipelines.yml` can be found [here](tests/.ci-pipelines.yml)

## Write a job or a module script using the run-job-script.sh component

In addition, to run a classical job (like [job-minimal](#job-minimal)), you can use the `run-job-script.sh` component to write a job into a modular format (Job parts, modules, ...) like the [job-advanced](#job-advanced) job example.

![Hook execution order](doc/HookExecutionOrder.png)

### Write a job

The job must be defined into `jobs/<my job name>.sh`

This job can use modules using `citbx_use "<my module>"`

The job can define the following functions (hooks) - Only applicable to a local workstation:

* `job_define()`: This function can be used to define options for use case on a workstation
* `job_setup()`: This function can be used to perform some action before setup and start the job environment (docker run)

The job can define the following functions (hooks) - applicable to all environments:

* `job_main()`: The job payload
* `job_after()`: This function can be used to perform some action after the job_main execution. This function is called in ALL case (success or error). This function is called with the process return code as the first argument (will be equal to 0 on success)

Example of job script: [ci/scripts/jobs/job-advanced.sh](ci/scripts/jobs/job-advanced.sh)

### Write a module

The module must be defined into `modules/<my module name>.sh`

This module can use other modules using `citbx_use "<my other module>"`

The module can define the following functions (hooks) - Only applicable to a local workstation:

* `citbx_module_<my module name>_define()`: This function can be used to define options for use case on a workstation
* `citbx_module_<my module name>_setup()`: This function can be used to perform some action before setup and start the job environment (docker run)

The job can define the following functions (hooks) - applicable to all environments:

* `citbx_module_<my module name>_before()`: This function can be used to perform some action before the job_main execution
* `citbx_module_<my module name>_after()`: This function can be used to perform some action after the job_main execution. This function is called in ALL case (success or error). This function is called with the return code of the last executed process from `job_main` function as the first argument (will be equal to 0 on success)

Example of module script: [ci/scripts/modules/example.sh](ci/scripts/modules/example.sh)

### Useful function list

* `citbx_job_list [prefix]`: Get the job list (optionally with the specified prefix)
* `citbx_use <module name>`: Load a module
* `citbx_export`: Export a variable to the job environment
* `citbx_subjob_export`: Export a variable to the sub jobs (in the context of the `--with-dependencies` option)
* `print_critical <message>`: Print an error message and exit (exit code: 1)
* `print_error <message>`: Print an error message
* `print_warning <message>`: Print a warning message
* `print_note <message>`: Print a note message
* `print_info <message>`: Print an in message

## Job examples

### job-minimal

Minimal dockerized job definition example

Job definition:

```yaml
image: ubuntu:16.04

job-minimal:
  stage: build
  script:
    - echo "Hello world"

after_script:
  - echo "job ${CI_JOB_NAME} end"
```

Job run:

![Job minimal](doc/TestCaseJobMinimal.png)

### job-with-before-after-script

Dockerized job definition example using `before_script` and `after_script`

Job definition:

```yaml
after_script:
  - echo "job ${CI_JOB_NAME} end"

job-with-before-after-script:
  stage: build
  before_script:
    - echo "executed before"
  script:
    - echo "script"
      "on several"
    - echo "lines"
    - cat <<< $(echo 'hi!')
  after_script:
    - echo "executed after"
```

### job-advanced

Dockerized job definition example (file [ci/scripts/jobs/job-advanced.sh](ci/scripts/jobs/job-advanced.sh)) with options, arguments, additional treatments and external module use (file [ci/scripts/modules/example.sh](ci/scripts/modules/example.sh))

Job definition:

```yaml
job-advanced:
  image: ubuntu:16.04
  stage: build
  variables:
    JOBADVAR: "${CI_JOB_NAME} JOBADVAR value"
  script:
    - echo ${GLOBALJOBVAR}
    - echo ${JOBADVAR}
    - ci/scripts/run-job-script.sh

after_script:
  - echo "job ${CI_JOB_NAME} end"
```

Job run:

![Job advanced](doc/TestCaseJobAvanced.png)

### job-test-services-mysql

Dockerized job definition example with a mysql service

Job definition:

```yaml
job-test-services-mysql:
  stage: build
  variables:
    MYSQL_DATABASE: test
    MYSQL_ROOT_PASSWORD: password
  image: mysql
  services:
    - mysql
  tags:
    - docker
  script:
    - printf "Waiting for mysql";
      while ! mysql -h mysql -u root -ppassword test -e '' > /dev/null 2>&1 ;
      do printf "."; sleep 1; done;
      printf " done!\n"
    - mysql -h mysql -u root -ppassword test -e 'SHOW VARIABLES LIKE "%version%";'
```

### job-test-services-postgres

Dockerized job definition example with a postgresql service

Job definition:

```yaml
job-test-services-postgres:
  stage: build
  image: postgres:9.4
  services:
    - name: postgres:9.4
      alias: db-postgres
      entrypoint: ["docker-entrypoint.sh"]
      command: ["postgres"]
  tags:
    - docker
  script:
    - printf "Waiting for postgres";
      while ! psql -h db-postgres -U postgres -c '' > /dev/null 2>&1 ;
      do printf "."; sleep 1; done;
      printf " done!\n"
    - psql -h db-postgres -U postgres -c 'select version();'
```
