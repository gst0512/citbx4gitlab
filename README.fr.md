# citbx4gitlab : Boîte à outils CI pour Gitlab

Cette boîte à outils peut être utilisée pour

* exécuter une tâche Gitlab en dehors d'un exécuteur de tâche Gitlab (outil `gitlab-runner`) sur votre poste de travail (fonction de base)
* écrire la partie script en utilisant une structure modulaire (fonction avancée)

## Table des matières

* [Le projet](#le-projet)
    * [Installation de l'outil ci-toolbox](#installation-de-loutil-ci-toolbox)
    * [Installation et configuration des prérequis](#installation-et-configuration-des-prérequis)
    * [Mise à jour de l'outil ci-toolbox](#mise-à-jour-de-loutil-ci-toolbox)
    * [Intégration optionnelle du composant run-job-script.sh dans une arborescence projet](#intégration-optionnelle-du-composant-run-job-scriptsh-dans-une-arborescence-projet)
* [Cas d'utilisation : Tâche de pipeline Gitlab standard](#cas-dutilisation--tâche-de-pipeline-gitlab-standard)
* [Exécuter une tâche spécifique localement](#exécuter-une-tâche-spécifique-localement)
    * [Première solution : utiliser l'outil gitlab-runner](#première-solution--utiliser-loutil-gitlab-runner)
    * [La seconde solution : la commande ci-toolbox](#la-seconde-solution--la-commande-ci-toolbox)
* [Commande pipeline](#commande-pipeline)
* [Écrire une tâche ou un module en utilisant le composant run-job-script.sh](#Écrire-une-tâche-ou-un-module-en-utilisant-le-composant-run-job-scriptsh)
    * [Écrire une tâche](#Écrire-une-tâche)
    * [Écriture d'un module](#Écriture-dun-module)
    * [Liste des fonctions utiles](#liste-des-fonctions-utiles)
* [Exemples de tâches](#exemples-de-tâches)
    * [job-minimal](#job-minimal)
    * [job-with-before-after-script](#job-with-before-after-script)
    * [job-advanced](#job-advanced)
    * [job-test-services-mysql](#job-test-services-mysql)
    * [job-test-services-postgres](#job-test-services-postgres)

## Le projet

### Installation de l'outil ci-toolbox

Vous pouvez installer cet outil en utilisant le paquet approprié depuis la dernière version [ici](https://gitlab.com/mbedsys/citbx4gitlab/-/releases).

### Installation et configuration des prérequis

La commande `ci-toolbox setup` permet d'installer l'ensemble des prérequis.
Tapez la commande `ci-toolbox setup -h` pour afficher l'aide associée ou `ci-toolbox setup -i` pour activer le mode interactif

### Mise à jour de l'outil ci-toolbox

La mise à jour s'effectue via la commande `ci-toolbox setup --component ci-toolbox` ou `ci-toolbox setup --component ci-toolbox <custom_version>`

### Intégration optionnelle du composant run-job-script.sh dans une arborescence projet

Cas d'intégration optionnelle du composant `run-job-script.sh` dans une arborescence projet :
```
├── .gitlab-ci.yml
├── .ci-toolbox.properties
├── ci/scripts
│   ├── modules
│   │   ├── ccache.sh
│   │   └── example.sh
│   ├── jobs
│   │   └── job-advanced.sh
│   ├── common.sh
│   └── run-job-script.sh
```

Liste des éléments :

* `.gitlab-ci.yml` : Définition du pipeline de tâches Gitlab
* `run-job-script.sh` : point d'entée pour le lancement du script de la tâche
* `.ci-toolbox.properties` : Propriétés de la boîte à outils propre au projet cible
* `jobs` : dossier contenant les fonctions spécifiques aux différentes tâches
* `modules` : dossier contenant les modules

## Cas d'utilisation : Tâche de pipeline Gitlab standard

Le schéma suivant décrit l'exécution d'une tâche standard appelée "J" dans un pipeline Gitlab-CI

![Cas d'utilisation global](doc/GitlabCIPipelineJob.png)

Dans ce cas, il serait intéressant d'exécuter une tâche spécifique comme J dans leur environnement approprié sur votre poste de travail local sans avoir à valider un enregistrement (`git commit`) ni avoir à pousser votre dépôt (`git push`).

Le but est d'avoir exactement le même environnement de construction sur l'exécuteur de tâche Gitlab et votre poste de travail local.

![Cas d'utilisation d'une tâche unique](doc/GitlabCISingleJob.png)

## Exécuter une tâche spécifique localement

### Première solution : utiliser l'outil gitlab-runner

Pour utiliser cette solution, vous devrez :

* installer l'outil `gitlab-runner` sur le poste de travail
* et le lancer de la manière suivante : `gitlab-runner exec <type d'exécuteur> <nom de la tâche>`

Vous devrez en plus :

* valider tous les changements locaux dans des enregistrement GIT
* ajouter des options supplémentaires à `gitlab-runner` du type `--docker-image` avec le nom de l'image appropriée

**NOTE :** La commande `gitlab-runner exec` est dépréciée depuis Gitlab 10, et donc susceptible de disparaître

### La seconde solution : la commande ci-toolbox

Cette boîte à outils est capable de le faire :

* démarrer une tâche  de type script ou `docker` avec les paramètres appropriés (image, etc.)
* lancer une tâche du pipeline localement sans avoir à valider un enregistrement (`git commit`)
* Lancer des pipelines (voir [Commande pipeline](#commande pipeline))

Fonctions additionnelles pour une utilisation avancée (avec l'utilisation de `run-job-script.sh`) :

* Ajouter des paramètres et des actions spécifiques au poste de travail
* Ajouter la possibilité d'exécuter une invite de commande dans l'environnement docker approprié
* Diviser les différentes tâche du pipeline de façon modulaire

Utilisation :

* `ci-toolbox <commande> [arguments...]` depuis n'importe quel endroit dans l'arborescence du projet

Les limites connues de la boîte à outils :

* Applicable uniquement pour les types d'exécuteur de tâche Gitlab `docker` et `shell`

## Commande pipeline

La commande pipeline (`ci-toolbox pipeline [arguments...]`) permet de :
* lancer le pipeline par défaut qui reprend la liste de toutes les tâches présentes dans le fichier `.gitlab-ci.yml` en utilisant la commande `ci-toolbox pipeline`
* lancer un pipeline spécifique/sur mesure en utilisant la commande `ci-toolbox pipeline -n <nom du pipeline>`

Les pipelines spécifiques doivent être spécifiés dans le fichier `.ci-pipelines.yml` en utilisant le format suivant :

```YAML
pipeline simple:
    - tâche 1
    - tâche 2
    - tâche 5

pipeline avancé:
    - tâche 3
    - tâche 5
    - name: tâche 2
      arguments:
        argument-1: "valeur 1"
        argument-2: ["première valeur", "deuxième valeur"]
```

Le document YAML est un objet composé de la manière suivante :
* chaque clé correspond à un nom de pipeline
* chaque valeur est un tableau contenant la liste des tâches

Chaque tâche peut être représentée par :
* Une chaine de caractère contenant le nom de la tâche
* Un objet contenant un camp `name` et `arguments` ave la liste des arguments

Un exemple de fichier `.ci-pipelines.yml` est disponible [ici](tests/.ci-pipelines.yml)

## Écrire une tâche ou un module en utilisant le composant run-job-script.sh

En plus de pouvoir lancer une tâche du pipeline, (comme la tâche [job-minimal](#job-minimal)), vous pouvez utiliser le composant `run-job-script.sh` pour écrire les scripts de celle-ci avec une structure modulaire comme c'est le cas pour la tâche d'exemple [job-advanced](#job-advanced)

![Ordre d'exécution des type de routines](doc/HookExecutionOrder.png)

### Écrire une tâche

Les routines spécifiques à la tâche en question doivent être écrites dans un script qui porte le nom de cette dernière dans le dossier : `jobs/<nom de la tâche>.sh`

Cette tâche peut faire appel à différents modules utilisant : `citbx_use "<nom du module>"`

La tâche peut définir les routines suivantes - Uniquement applicables à un poste de travail local :

* `job_define()` : Cette fonction peut être utilisée pour définir des options pour le cas d'utilisation sur un poste de travail
* `job_setup()` : Cette fonction peut être utilisée pour effectuer une action avant de configurer et démarrer l'environnement de la tâche (docker run)

La tâche peut définir les routines suivantes - applicables à tous les environnements :

* `job_main()` : La fonction principale de la tâche qui contient la charge utile
* `job_after()` : Cette fonction peut être utilisée pour effectuer une action après l'exécution de la fonction `job_main`. Cette fonction est appelée dans TOUS les cas (succès ou erreur). Cette fonction est appelée avec le code de retour du processus comme premier argument (sera égal à 0 sur succès)

Exemple de script de tâche : [ci/scripts/jobs/job-advanced.sh](ci/scripts/jobs/job-advanced.sh)

### Écriture d'un module

Le module doit être défini dans `modules/<nom de mon module>.sh`.

Ce module peut utiliser d'autres modules à l'aide de `citbx_use'"<mon autre module>"`.

Le module peut définir les routines suivantes - Uniquement applicables à un poste de travail local :

* `citbx_module_<nom de mon module>_define()` : Cette fonction peut être utilisée pour définir des options pour le cas d'utilisation sur un poste de travail
* `citbx_module_<nom de mon module>_setup()` : Cette fonction peut être utilisée pour effectuer une action avant de configurer et démarrer l'environnement de la tâche (`docker run`)

La tâche peut définir les routines suivantes - applicables à tous les environnements :

* `citbx_module_<nom de mon module>_before()` : Cette fonction peut être utilisée pour réaliser des actions avant l'exécution de la fonction principale de la tâche `job_main`
* `citbx_module_<nom de mon module>_after()` : Cette fonction peut être utilisée pour réaliser des actions après l'exécution de la fonction principale de la tâche `job_main`. Cette fonction est appelée dans tous les cas (succès ou erreur) avec comme premier argument le code de retour du dernier processus exécuté dans la fonction `job_main` (sera égal à 0 en cas de succès)

Exemple de script de module : [ci/scripts/modules/example.sh](ci/scripts/modules/example.sh)

### Liste des fonctions utiles

* `citbx_job_list [prefix]` : Obtenir la liste des tâches (en option : avec le préfixe spécifié)
* `citbx_use <nom du module>` : Charger un module
* `citbx_export`: Exporte une variable à l'environnement de la tâche
* `citbx_subjob_export`: Exporte une variable à l'environnement des sous tâche (dans le contexte de l'option `--with-dependencies`)
* `print_critical <message>` : Affichage d'un message d'erreur et sortie (code de retour : 1)
* `print_error <message>` : Affichage un message d'erreur
* `print_warning <message>` : Affichage un message d'avertissement
* `print_note <message>` : Affichage un message de note
* `print_info <message>` : Affichage un message d'information

## Exemples de tâches

### job-minimal

Exemple de de tâche docker minimaliste

Définition de la tâche :

```yaml
image: ubuntu:16.04

job-minimal:
  stage: all-in-one
  script:
    - echo "Bonjour monde !"

after_script:
  - echo "job ${CI_JOB_NAME} end"
```

Exécution de la tâche :

![Tâche minimaliste](doc/TestCaseJobMinimal.png)

### job-with-before-after-script

Exemple de définition de tâche docker avec les propriétés `before_script` et `after_script`

Définition de la tâche :

```yaml
after_script:
  - echo "job ${CI_JOB_NAME} end"

job-with-before-after-script:
  stage: all-in-one
  before_script:
    - echo "exécuté avant"
  script:
    - echo "script"
      "sur plusieurs"
    - echo "lignes"
    - cat <<< $(echo 'salut !')
  after_script:
    - echo "exécuté après"
```

### job-advanced

Exemple de définition d'une tâche docker (fichier [ci/scripts/jobs/job-advanced.sh](ci/scripts/jobs/job-advanced.sh)) avec options, arguments, traitements supplémentaires et utilisation de modules externes (fichier [ci/scripts/modules/example.sh](ci/scripts/modules/example.sh))

Définition de la tâche :

```yaml
job-advanced:
  image: ubuntu:16.04
  stage: all-in-one
  variables:
    JOBADVAR: "${CI_JOB_NAME} JOBADVAR value"
  script:
    - echo ${GLOBALJOBVAR}
    - echo ${JOBADVAR}
    - ci/scripts/run-job-script.sh

after_script:
  - echo "job ${CI_JOB_NAME} end"
```

Exécution de la tâche :

![Tâche avancée](doc/TestCaseJobAvanced.png)

### job-test-services-mysql

Exemple de définition de tâche docker avec un service MySQL

Définition de la tâche :

```yaml
job-test-services-mysql:
  stage: build
  variables:
    MYSQL_DATABASE: test
    MYSQL_ROOT_PASSWORD: password
  image: mysql
  services:
    - mysql
  tags:
    - docker
  script:
    - printf "Waiting for mysql";
      while ! mysql -h mysql -u root -ppassword test -e '' > /dev/null 2>&1 ;
      do printf "."; sleep 1; done;
      printf " done!\n"
    - mysql -h mysql -u root -ppassword test -e 'SHOW VARIABLES LIKE "%version%";'
```

### job-test-services-postgres

Exemple de définition de tâche docker avec un service PostgreSQL

Définition de la tâche :

```yaml
job-test-services-postgres:
  stage: build
  image: postgres:9.4
  services:
    - name: postgres:9.4
      alias: db-postgres
      entrypoint: ["docker-entrypoint.sh"]
      command: ["postgres"]
  tags:
    - docker
  script:
    - printf "Waiting for postgres";
      while ! psql -h db-postgres -U postgres -c '' > /dev/null 2>&1 ;
      do printf "."; sleep 1; done;
      printf " done!\n"
    - psql -h db-postgres -U postgres -c 'select version();'
```
