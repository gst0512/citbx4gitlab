#!/usr/bin/env python3
# citbx4gitlab: CI toolbox for Gitlab
# Copyright (C) 2021 MBEDSYS - Emeric Verschuur <emeric@mbedsys.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""GitLab .gitlab-ci.yml file processor"""

import copy
import json
import os
import re
import sys
import urllib.parse as urlparse
import urllib.request as urlreq
import yaml

if "CI_PROJECT_DIR" not in os.environ:
    sys.exit()
CI_PROJECT_DIR = os.environ.get("CI_PROJECT_DIR")

DIRECT_JOB_DEPS = bool("CITBX_JOB_EXPLICIT_DEPS_ONLY" in os.environ \
        and os.environ.get("CITBX_JOB_EXPLICIT_DEPS_ONLY") == "true")

if "CI_API_V4_URL" in os.environ:
    CI_API_V4_URL = os.environ.get("CI_API_V4_URL")

if "CI_JOB_TOKEN" in os.environ:
    CI_JOB_TOKEN = os.environ.get("CI_JOB_TOKEN")

if "CITBX_HTTP_USER_AGENT" in os.environ:
    CITBX_HTTP_USER_AGENT = os.environ.get("CITBX_HTTP_USER_AGENT")
else:
    CITBX_HTTP_USER_AGENT = 'curl/7.68.0'

if "CITBX_TEMPLATE_API_URL" in os.environ:
    CITBX_TEMPLATE_API_URL = os.environ.get("CITBX_TEMPLATE_API_URL")
else:
    CITBX_TEMPLATE_API_URL = "https://gitlab.com/api/v4"

if "CITBX_TEMPLATE_PROJECT_ID" in os.environ:
    CITBX_TEMPLATE_PROJECT_ID = os.environ.get("CITBX_TEMPLATE_PROJECT_ID")
else:
    CITBX_TEMPLATE_PROJECT_ID = "gitlab-org/gitlab"

if "CITBX_TEMPLATE_PROJECT_REF" in os.environ:
    CITBX_TEMPLATE_PROJECT_REF = os.environ.get("CITBX_TEMPLATE_PROJECT_REF")
else:
    CITBX_TEMPLATE_PROJECT_REF = "master"

if "CITBX_TEMPLATE_DIR" in os.environ:
    CITBX_TEMPLATE_DIR = os.environ.get("CITBX_TEMPLATE_DIR")
else:
    CITBX_TEMPLATE_DIR = "lib/gitlab/ci/templates"

DEPRECATED_KEYWORDS = {}

class Error(Exception):
    """Error message"""

def update_dict(dst, src):
    """Update a dict with an other"""
    for key, value in src.items():
        if isinstance(value, dict):
            dst[key] = update_dict(dst.get(key, {}), value)
        else:
            dst[key] = value
    return dst

def shell_str(text):
    """Escape a string character to be evaluated in bash os.environment"""
    return "'" + text.replace("'", "'\\''") + "'"

def shell_str_ext(text):
    """Escape a string character with bash variables to be evaluated in bash os.environment"""
    return re.sub(r'((?<!\$)\$([a-zA-Z0-9_]+|\{[a-zA-Z0-9_]+\}))', "'\"\\1\"'", \
            shell_str(text)).replace("$$", "$")

def http_get(url):
    """Get file from an HTTP ressource"""
    req = urlreq.Request(url)
    req.add_header('User-Agent', CITBX_HTTP_USER_AGENT)
    if url.startswith(CI_API_V4_URL) and 'CI_JOB_TOKEN' in globals():
        req.add_header('Authorization', 'Bearer ' + CI_JOB_TOKEN)
    return urlreq.urlopen(req)

# pylint: disable=R0912
def read_gitlab_ci_yml_inc(inc):
    """Process GitLab-CI YAML file [include] node"""
    if isinstance(inc, str):
        return read_gitlab_ci_yml_file(inc)
    if isinstance(inc, dict):
        if 'remote' in inc:
            return read_gitlab_ci_yml_file(inc['remote'])
        if 'local' in inc:
            return read_gitlab_ci_yml_file(inc['local'])
        if 'project' in inc:
            if not 'CI_API_V4_URL' in globals():
                raise Error("To use [project] import, you must define CI_API_V4_URL" \
                    + " os.environment variable")
            if not isinstance(inc['project'], str):
                raise Error("[project] attribute must contains a character string")
            project = inc['project']
            if 'ref' in inc:
                if not isinstance(inc['ref'], str):
                    raise Error("[ref] attribute must contains a character string")
                ref = inc['ref']
            else:
                result = json.loads(http_get(CI_API_V4_URL + "/projects/" \
                    + urlparse.quote_plus(project)).read())
                if not 'default_branch' in result:
                    raise Error("Unable to get default branch name from [" + project \
                        + "] projet")
                ref = result['default_branch']
            if not 'file' in inc:
                raise Error("missing [file] attribute for a [project] include")
            if isinstance(inc['file'], str):
                file_list = [inc['file']]
            elif isinstance(inc['file'], list):
                file_list = inc['file']
            else:
                raise Error("[file] attribute must contains a character string or " \
                    + "a character string list")
            inc_result = {}
            for file_path in file_list:
                inc_result = update_dict(inc_result, read_gitlab_ci_yml_file(CI_API_V4_URL \
                    + "/projects/" + urlparse.quote_plus(project) + "/repository/files/" \
                        + urlparse.quote_plus(re.sub(r'^[/]*', '', file_path)) \
                            + "/raw?ref=" + urlparse.quote_plus(ref)))
            return inc_result
        if 'template' in inc:
            if not isinstance(inc['template'], str):
                raise Error("[template] attribute must contains a character string")
            template = inc['template']
            return read_gitlab_ci_yml_file(CITBX_TEMPLATE_API_URL + "/projects/" \
                + urlparse.quote_plus(CITBX_TEMPLATE_PROJECT_ID) \
                    + "/repository/files/" + urlparse.quote_plus(CITBX_TEMPLATE_DIR + "/" \
                        + re.sub(r'^[/]*', '', template)) \
                            + "/raw?ref=" + urlparse.quote_plus(CITBX_TEMPLATE_PROJECT_REF))
        raise Error("Only [remote], [project], [template] and " \
            + "[local] import supported here!")
    if isinstance(inc, list):
        inc_result = {}
        for inc_elt in inc:
            inc_result = update_dict(inc_result, read_gitlab_ci_yml_inc(inc_elt))
        return inc_result
    raise Error("Invalid include entry: " + str(inc))
# pylint: enable=R0912

def read_gitlab_ci_yml_file(ci_file_path):
    """Read one GitLab-CI YAML file"""
    try:
        if ci_file_path.startswith(("http://", "https://")):
            document_root = yaml.safe_load(http_get(ci_file_path))
        else:
            file_d = open(CI_PROJECT_DIR + '/' + ci_file_path)
            document_root = yaml.safe_load(file_d.read())
        if not isinstance(document_root, dict):
            raise Error("Invalid document format")
        if 'include' in document_root:
            document = read_gitlab_ci_yml_inc(document_root['include'])
        else:
            document = {}
        return update_dict(document, document_root)
    except Exception as err:
        raise Error("Unable to load GitLab-CI YAML [" + ci_file_path + "] file!" \
            + os.linesep + os.linesep + "Raison:" + os.linesep + str(err))

def node_process_extends(nodes, node_name, dep_list):
    """Resolve the [extends] section of a job node"""
    try:
        node = nodes[node_name]
        if not 'extends' in node:
            return nodes
        dep_list.append(node_name)
        extends = node['extends']
        node.pop('extends', None)
        if isinstance(extends, str):
            extends = [extends]
        if not isinstance(extends, list):
            raise Error("[extends] attribute must contains only " \
                + "character string and array of character string")
        for extend in extends:
            if extend == node_name:
                raise Error("a node cannot extends itself")
            if extend in dep_list:
                raise Error("circular dependency detected in [extends]")
            if extend in nodes:
                node_process_extends(nodes, extend, dep_list.copy())
                node = update_dict(copy.deepcopy(nodes[extend]), node)
            else:
                raise Error("Unknown key [" + extend + "]")
        nodes[node_name] = node
        return nodes
    except Error as err:
        raise Error("during [" + node_name + "] node process, " + str(err))

def read_gitlab_ci_yml(ci_file_path):
    """Read and GitLab-CI YAML file and it's dependencies and resolve all [extends] sections"""
    document = read_gitlab_ci_yml_file(ci_file_path)
    for node_name in document:
        document = node_process_extends(document, node_name, [])
    return document

def get_job_node(document, job_name):
    """Get a job node from the document"""
    if not job_name in document:
        raise Error("Undefined job [" + job_name + "]!")
    if not isinstance(document[job_name], dict):
        raise Error("Invalid job node [" + job_name + "]!")
    return document[job_name]

DEFAULT_STAGE_LIST = ["build", "test", "deploy"]

# pylint: disable=R0912
def get_job_tree(document):
    """Build the job tree (list of job grouped by stage)"""
    job_tree = {}
    try:
        if 'stages' in document:
            stages_node = document['stages']
        elif 'types' in document:
            # NOTE: Globally defined [types] keyword is deprecated
            DEPRECATED_KEYWORDS['types'] = True
            stages_node = document['types']
        else:
            stages_node = DEFAULT_STAGE_LIST
        if not isinstance(stages_node, list):
            raise Error()
        for stage in stages_node:
            if not isinstance(stage, str):
                raise Error()
            job_tree[stage] = []
    except Error:
        raise Error("[stages] element must be an array of character string")
    jobs = []
    for key, value in document.items():
        if key[0] == '.' or not isinstance(value, dict):
            continue
        job_node = get_job_node(document, key)
        if 'script' in job_node and isinstance(job_node['script'], (list, str)):
            jobs.append(key)
            if 'stage' in job_node and isinstance(job_node['stage'], str):
                stage = job_node['stage']
            elif 'type' in job_node and isinstance(job_node['type'], str):
                # NOTE: Job defined [type] keyword is deprecated
                DEPRECATED_KEYWORDS['type'] = key
                stage = job_node['type']
            else:
                raise Error("[stage] not found for job [" + key + "]!")
            if not stage in job_tree:
                raise Error("Job [" + key + "]: Undefined stage [" + stage + "]")
            job_tree[stage].append(key)
    for keyword in ["image", "services", "cache", "before_script", "after_script"]:
        if keyword in document:
            DEPRECATED_KEYWORDS[keyword] = True
    return job_tree
# pylint: enable=R0912

def list_stages_and_jobs(document):
    """Export the job and stage list"""
    job_tree = get_job_tree(document)
    print("CITBX_STAGE_LIST=()")
    print("CITBX_JOB_LIST=()")
    for key, value in job_tree.items():
        print("CITBX_STAGE_LIST+=(" + shell_str(key) + ")")
        print("CITBX_HELP_JOB_LIST+=(" + shell_str('Stage [' + key + ']:') + ")")
        for job in sorted(value):
            print("CITBX_JOB_LIST+=(" + shell_str(job) + ")")
            print("CITBX_HELP_JOB_LIST+=(" + shell_str(' * ' + job) + ")")

def process_variables_node_attr(variables, ref_node):
    """Retrieve a job variable list"""
    if 'variables' in ref_node and isinstance(ref_node['variables'], dict):
        if not ref_node['variables']:
            return {}
        for key, value in ref_node['variables'].items():
            if isinstance(value, (list, dict)):
                raise Error("Variable element cannot be an array or an object")
            variables[key] = str(value)
    return variables

def process_job_script_node(node, node_name, accept_script):
    """Retrieve a job script part"""
    if accept_script and isinstance(node, str):
        return [node]
    if not isinstance(node, list):
        raise Error("Invalid [" + node_name + "] element!")
    items = []
    for elt in node:
        items = items + process_job_script_node(elt, node_name, True)
    return items

def process_job_script_items(document, default_node, job_node, script_node_name):
    """Retrieve job script items"""
    if script_node_name == 'script':
        if 'script' not in job_node:
            raise Error("[script] element not found!")
        return process_job_script_node(job_node['script'], 'script', True)
    if script_node_name in job_node:
        return process_job_script_node(job_node[script_node_name], script_node_name, False)
    if script_node_name in default_node:
        return process_job_script_node(default_node[script_node_name], script_node_name, False)
    if script_node_name in document:
        # NOTE: Globally defined [before_script] and [after_script] keyword is deprecated
        DEPRECATED_KEYWORDS[script_node_name] = True
        return process_job_script_node(document[script_node_name], script_node_name, False)
    return []

def process_job_services(document, default_node, job_node):
    """Retrieve job service properties"""
    if 'services' in job_node:
        services_node = job_node['services']
    elif 'services' in default_node:
        services_node = default_node['services']
    elif 'services' in document:
        # NOTE: Globally defined [services] keyword is deprecated
        DEPRECATED_KEYWORDS['services'] = True
        services_node = document['services']
    else:
        return []
    if not isinstance(services_node, list):
        raise Error("[services] element must be an array")
    computed_list = []
    for node in services_node:
        if isinstance(node, str):
            computed_list.append({'name': node})
            continue
        # pylint: disable=R0916
        if not isinstance(node, dict) or not 'name' in node or not isinstance(node['name'], str) \
                    or ('entrypoint' in node and not isinstance(node['entrypoint'], (str, list))) \
                    or ('command' in node and not isinstance(node['command'], (str, list))) \
                    or ('alias' in node and not isinstance(node['alias'], str)):
            raise Error("Invalid node " + str(node) + " in [services] element!")
        # pylint: enable=R0916
        if 'entrypoint' in node and isinstance(node['entrypoint'], list):
            node['entrypoint'] = " ".join(node['entrypoint'])
        if 'command' in node and isinstance(node['command'], list):
            node['command'] = " ".join(node['command'])
        computed_list.append(node)
    return computed_list

def process_job_image(document, default_node, job_node):
    """Retrieve job image properties"""
    if 'image' in job_node:
        image_node = job_node['image']
    elif 'image' in default_node:
        image_node = default_node['image']
    elif 'image' in document:
        # NOTE: Globally defined [image] keyword is deprecated
        DEPRECATED_KEYWORDS['image'] = True
        image_node = document['image']
    else:
        return {}
    if isinstance(image_node, str):
        return {'name': image_node}
    if not isinstance(image_node, dict):
        raise Error("Invalid [image] element!")
    if 'name' in image_node and not isinstance(image_node['name'], str):
        raise Error("[image.name] element must be a character string")
    if 'entrypoint' in image_node:
        if isinstance(image_node['entrypoint'], list):
            image_node['entrypoint'] = " ".join(image_node['entrypoint'])
        elif not isinstance(image_node['entrypoint'], str):
            raise Error("[image.entrypoint] element must be a character string or an " \
                + "array of character string")
    return image_node

def process_job_dependencies(document, job_node, job_list, stage2debs):
    """Build the job dependency tree into a flat list"""
    dep_list = []
    if 'needs' in job_node and isinstance(job_node['needs'], list):
        for need in job_node['needs']:
            if isinstance(need, str):
                dep_list.append(need)
            elif isinstance(need, dict) and 'job' in need and isinstance(need['job'], str):
                dep_list.append(need['job'])
    if 'dependencies' in job_node and isinstance(job_node['dependencies'], list):
        for dep in job_node['dependencies']:
            if isinstance(dep, str):
                dep_list.append(dep)
    if not dep_list and not DIRECT_JOB_DEPS:
        dep_list = stage2debs[job_node['stage']]
    for dep in dep_list:
        if dep in job_list:
            continue
        job_list.append(dep)
        job_node = get_job_node(document, dep)
        job_list = process_job_dependencies(document, job_node, job_list, stage2debs)
    return job_list

# pylint: disable=R0912,R0914
def process_job(document, ci_job_name):
    """Process a specific job"""
    # Global variables collection
    variables = process_variables_node_attr({}, document)
    # Get global image if exists
    if 'image' in document and isinstance(document['image'], str):
        image = document['image']
    # Get job root node
    job_node = get_job_node(document, ci_job_name)
    # Get default node
    if 'default' in document and isinstance(document['default'], dict):
        default_node = document['default']
    else:
        default_node = {}
    # Job variables collection
    variables = process_variables_node_attr(variables, job_node)
    # Get job image info
    image = process_job_image(document, default_node, job_node)
    # Get job image info
    services = process_job_services(document, default_node, job_node)
    # Get script items
    script_items = process_job_script_items(document, default_node, job_node, 'before_script') \
            + process_job_script_items(document, default_node, job_node, 'script')
    after_script_items = process_job_script_items(document, default_node, job_node, 'after_script')
    # Get job dependencies
    job_tree = get_job_tree(document)
    stage2debs = {}
    deps = []
    for key, value in job_tree.items():
        stage2debs[key] = deps
        deps = value
    dependency_list = process_job_dependencies(document, job_node, [], stage2debs)
    dependency_list.reverse()

    # Export job info
    print("CITBX_JOB_VARIABLE_LIST=()")
    for key, value in variables.items():
        print("CITBX_JOB_VARIABLE_LIST+=(" + key + ")")
        print(key + "=" + shell_str_ext(value))
    if 'name' in image:
        print("CITBX_JOB_IMAGE_NAME=" + shell_str_ext(image['name']))
    if 'entrypoint' in image:
        print("CITBX_JOB_IMAGE_ENTRYPOINT=" + shell_str_ext(image['entrypoint']))
    if 'stage' in job_node:
        print("CI_JOB_STAGE=" + shell_str_ext(job_node['stage']))
    print("CITBX_JOB_SERVICE_LIST=()")
    for service in services:
        value = "image_name=" + shell_str_ext(service['name']) + os.linesep
        if 'alias' in service:
            value += "alias=" + shell_str_ext(service['alias']) + os.linesep
        if 'command' in service:
            value += "command=" + shell_str_ext(service['command']) + os.linesep
        if 'entrypoint' in service:
            value += "entrypoint=" + shell_str_ext(service['entrypoint']) + os.linesep
        print("CITBX_JOB_SERVICE_LIST+=(" + shell_str(value) + ")")
    print("CITBX_JOB_DEPENDENCIES_LIST=()")
    for dep in dependency_list:
        print("CITBX_JOB_DEPENDENCIES_LIST+=(" + shell_str(dep) + ")")
    script_lines = []
    for item in script_items:
        script_lines.append(r'printf "\033[1m\033[92m$ %s\033[0m\n" ' + shell_str(item))
        script_lines.append(item)
    print("CITBX_JOB_SCRIPT=" + shell_str(shell_str("\n".join(script_lines))))
    if after_script_items:
        script_lines = []
        for item in after_script_items:
            script_lines.append(r'printf "\033[1m\033[92m$ %s\033[0m\n" ' + shell_str(item))
            script_lines.append(item)
        print("CITBX_JOB_AFTER_SCRIPT=" + shell_str(shell_str("\n".join(script_lines))))
# pylint: enable=R0912,R0914

def process_deprecated_keywords():
    """Process deprecated used keyword"""
    errors = []
    for key, value in DEPRECATED_KEYWORDS.items():
        if key == "type":
            errors.append(" * In the job [" + value + "]: [type] is deprecated, " \
                + "and could be removed in one of the future releases. Use stage instead")
        if key == "types":
            errors.append(" * [types] is deprecated, and could be removed in a future release." \
                + " Use [stages] instead.")
        if key in ["image", "services", "cache", "before_script", "after_script"]:
            errors.append(" * Defining [image], [services], [cache], [before_script], and " \
                + "[after_script] globally is deprecated. Use [default:] instead.")
    if not errors:
        return
    out_result = ["print_warning"]
    out_result.append(shell_str("One or several deprecated keyword(s) is used in your GitLab-CI" \
        + " configuration:"))
    for line in errors:
        out_result.append(shell_str(line))
    out_result.append(shell_str(" => Please visit https://docs.gitlab.com/ee/ci/yaml/README.html" \
        + "#deprecated-keywords to get more information"))
    print(" ".join(out_result))

def main():
    """Main function"""
    if "CI_CONFIG_PATH" in os.environ:
        ci_config_path = os.environ.get("CI_CONFIG_PATH")
    else:
        ci_config_path = ".gitlab-ci.yml"

    try:
        document = read_gitlab_ci_yml(ci_config_path)
        if "CI_JOB_NAME" in os.environ:
            process_job(document, os.environ.get("CI_JOB_NAME"))
        else:
            list_stages_and_jobs(document)
        process_deprecated_keywords()
    except Error as err:
        out_result = ["print_critical"]
        for line in str(err).splitlines():
            out_result.append(shell_str(line))
        print(" ".join(out_result))
        if "CITBX_YAML2BASH_DEBUG" in os.environ \
            and os.environ.get("CITBX_YAML2BASH_DEBUG") == "true":
            raise err


if __name__ == "__main__":
    main()
