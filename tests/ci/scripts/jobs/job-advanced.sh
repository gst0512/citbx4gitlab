
citbx_use "example"

job_define() {
    # You can: Define options:
    bashopts_declare -n JOB_OPTION_LOCAL -l job-opt-local \
        -d "Local job option not accessible from the docker" -s -i -t string -v "my local value"
    bashopts_declare -n JOB_OPTION_EXPORT -l job-opt-export \
        -d "Job option accessible from the docker" -s -i -t string -v "my exported value"
    bashopts_declare -n JOB_OPTION_EXIT_CODE -l job-exit-code \
        -d "Job exit code" -i -t number -v "0"
    # Add JOB_OPTION_EXPORT to the export list
    CITBX_ENV_EXPORT_LIST+=(JOB_OPTION_EXPORT JOB_OPTION_EXIT_CODE)
}

# You can: Define a setup hook:
job_setup() {
    print_info "=> This part is executed:" \
            "   * only ouside a gilab runner (e.g.: on the devloper workstation)" \
            "   * on the host (outside the docker container)" \
            "   * before start the suitable docker container"
    print_note "Outside the docker:" \
        "* JOB_OPTION_LOCAL=$JOB_OPTION_LOCAL" \
        "* JOB_OPTION_EXPORT=$JOB_OPTION_EXPORT"
    # Test variable scope
    test "$JOB_TEST_VAR" == "job-advanced - scope: global"
}

job_main() {
    print_info "This part is executed into the suitable docker container"
    print_note "Inside the docker:" \
               "* JOB_OPTION_LOCAL=$JOB_OPTION_LOCAL" \
               "* JOB_OPTION_EXPORT=$JOB_OPTION_EXPORT" \
               "* JOB_UNEVALUED_VAR=$JOB_UNEVALUED_VAR" \
               "* JOB_EVALUED_VAR=$JOB_EVALUED_VAR"
    exit $JOB_OPTION_EXIT_CODE
}

job_after() {
    if [ $1 -eq 0 ]; then
        print_info "job_finish: OK"
    else
        print_error "job_finish: KO (CODE: $1)"
    fi
}
