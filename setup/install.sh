#!/bin/bash -e

set -eo pipefail

fetch_dist_file() {
    wget -O "$1" "$2"
}

install_pkg_deb() {
    sudo apt install --reinstall --allow-downgrades -y "$@"
}

install_pkg_rpm() {
    # TODO 1: Find a way to add --reinstall option that works on all cases (install & reinstall)
    # TODO 2: Find an equivalent to the apt 'allow-downgrades' option
    sudo dnf install -y "$@"
}

install_pkg_tar() {
    sudo tar -C /usr/local -xf "$@"
}

setup_pkg() {
    local pkg_type=$1
    mkdir -p /tmp
    fetch_dist_file /tmp/ci-toolbox.$pkg_type \
        https://gitlab.com/api/v4/projects/mbedsys%2fcitbx4gitlab/packages/generic/ci-toolbox/$CITBX_GIT_VERSION/ci-toolbox-$CITBX_GIT_VERSION.$pkg_type
    install_pkg_$pkg_type /tmp/ci-toolbox.$pkg_type
}

if [ -z "$1" ] || [ "$1" == "master" ]; then
    CITBX_GIT_VERSION=$(curl -s https://gitlab.com/api/v4/projects/mbedsys%2fcitbx4gitlab/releases | jq -r '.[0].tag_name')
else
    CITBX_GIT_VERSION=$1
fi

if [ -f /etc/debian_version ]; then
    # Debian based system
    setup_pkg deb
elif [ -f /etc/redhat-release ]; then
    # Redhat based system
    setup_pkg rpm
else
    setup_pkg tar
fi
